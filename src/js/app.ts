import { fighterService } from "./services/fighters.service";
import { IFighter } from "./fighter";
import Model, { IModel } from "./models/model";
import GameScreen from "./gameScreen";
import Fight from "./fightScreen";


export interface IApp {
  model: IModel;
  startApp: () => void;
}

export default class App implements IApp {
  model: IModel;
  static loadingElement = document.getElementById('loading-overlay') as HTMLElement;
  static rootElement = document.getElementById('root') as HTMLElement;

  constructor() {
    this.startApp();
  }

  async startApp() {
    try {
      const fighters: Array<IFighter> = await fighterService.getFighters();
      this.model = new Model(fighters);
      App.showGameScreen(this.model);

    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

  static showGameScreen(model: IModel) {
    new GameScreen(model);
  }

  static showFightScreen(fighters: IFighter[]) {
    const fight = new Fight(fighters);
    App.rootElement.innerHTML = '';
    App.rootElement.append(fight.element);
    fight.start();
  }

  static showLoader() {
    const loader = document.createElement('div');
    loader.className = 'loader';
    loader.id = 'loader';
    document.body.append(loader);
  }

  static hideLoader() {
    document.getElementById('loader').remove();
  }
}