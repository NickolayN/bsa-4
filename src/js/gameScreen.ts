import { IFighter } from "./fighter";
import { IModel, Selected } from "./models/model";
import FightersView from "./views/fightersView";
import MenuView, { IMenuView } from "./views/menuView";
import App from "./app";
import { fighterService } from "./services/fighters.service";
import ModalView from "./views/modalView";

export interface IGameScreen {
  element: HTMLElement;
}

export default class GameScreen implements IGameScreen {
  element: HTMLElement;
  private _model: IModel;
  private _menuView: IMenuView;
  private _modalElem: HTMLElement;

  constructor(model: IModel) {
    this._model = model;

    this._showMenu(this._model.getSelected());
    this._showFighters(this._model.fighters);
  }

  private _showMenu(selected: Selected[]) {
    this._menuView = new MenuView(selected);;
    this._menuView.updateMenu(selected);
    App.rootElement.append(this._menuView.element);
  }

  private _updateMenu(selected: Selected[]) {
    this._menuView.updateMenu(selected);
    this._menuView.onFighterClick = this._handleFighterClick.bind(this);
    this._menuView.onStartClick = this._handleStart.bind(this);
  }

  private _showFighters(fighters: IFighter[]) {
    const fightersView = new FightersView(this._model.fighters);
    fightersView.onClick = this._handleSelect.bind(this);
    App.rootElement.append(fightersView.element);
  }

  private _handleFighterClick(id: number) {
    this._model.deselect(id);
    this._updateMenu(this._model.getSelected());
  }

  private async _handleSelect(id: number) {
    this._model.select(id);
    const selected = this._model.getSelected();
    this._updateMenu(selected);

    const fighter = this._model.getFighter(id);
    if (!fighter.attack) {
      const details = await fighterService.getFighterDetails(id);
      this._showModal(details);
    } else {
      this._showModal(fighter);
    }
  }

  private _showModal(fighter: IFighter) {
    const modal = new ModalView(fighter);
    modal.onSubmit = this._handleSubmit.bind(this);
    this._modalElem = modal.element;
    
    App.showLoader();
    App.rootElement.append(this._modalElem);
  }

  private _closeModal(elem: HTMLElement) {
    App.hideLoader();
    elem.remove();
  }

  private _handleSubmit(fighter: IFighter) {
    this._model.addFighter(fighter);
    this._closeModal(this._modalElem);
  }

  private _handleStart() {
    console.log('start');
    App.showFightScreen(this._model.getSelected());
  }
}