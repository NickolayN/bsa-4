import { callApi } from "../helpers/api.helper";
import { IFighter } from "../fighter";

export interface IFighterService {
  getFighters: () => Array<IFighter>;
  getFighterDetails: (_id: number) => IFighter;
}

class FighterService {
  getFighters() {
    const endpoint: string = 'fighters.json';
    return this._getData(endpoint);
  }

  getFighterDetails(_id: number) {
    const endpoint = `details/fighter/${_id}.json`;

    return this._getData(endpoint);
  }

  async _getData(endpoint: string) {
    try {      
      const apiResult: any = await callApi(endpoint, 'GET');
      return apiResult;

      // return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();