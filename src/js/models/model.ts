import { IFighter } from "../fighter";

type State = {
  fighters: Map<number, IFighter>;
  selected: number[];
};
export type Selected = IFighter | null;

export interface IModel {
  fighters: Array<IFighter>;
  getSelected: () => Array<Selected>;
  getFighter: (id: number) => IFighter;
  addFighter: (fighter: IFighter) => void;
  select: (id: number) => void;
  deselect: (id: number) => void;
}

export default class Model {
  private _state: State;

  constructor(fighters: Array<IFighter>) {
    this._state = {
      fighters: new Map(fighters.map(fighter => [Number(fighter._id), fighter])),
      selected: [null, null]
    }
  }

  get fighters() {
    return [...this._state.fighters.values()];
  }

  set fighters(fighters: Array<IFighter>) {
    const map = new Map(fighters.map(fighter => [Number(fighter._id), fighter]));
    this._state = {...this._state, fighters: map};
  }

  getSelected() {
    const [first, second]= this._state.selected;
    return [this.getFighter(first), this.getFighter(second)];
  }

  getFighter(id: number) {
    return this._state.fighters.get(id);
  }

  addFighter(fighter: IFighter) {
    this._state.fighters.set(Number(fighter._id), fighter);
  }

  select(id: number) {
    if (this._state.selected[0]) {
      this._state.selected[1] = id;
    } else {
      this._state.selected[0] = id;
    }
  }

  deselect(id: number) {
    const selected = this._state.selected.map(fighter => fighter == id ? null : fighter);
    this._state = { ...this._state,  selected };
  }
}