import Fighter from "../fighter";

export function calculateDamage(attacker: Fighter, defender: Fighter): number {
  return attacker.getHitPower() - defender.getBlockPower();
}