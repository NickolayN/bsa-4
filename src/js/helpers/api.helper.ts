const API_URL: string = 'http://localhost:8080/resources/api/';

export function callApi(endpoint: string, method: string) {
  const url: string = API_URL + endpoint;
  const options = {
    method
  };

  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .catch(error => {
      throw error;
    });
}