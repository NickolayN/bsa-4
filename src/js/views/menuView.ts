import View, { IView } from './view';
import { IFighter } from '../fighter';
import { Selected } from "../models/model";

export interface IMenuView extends IView {
  element: HTMLElement;
  updateMenu: (selected: Array<Selected>) => void;
  onFighterClick: (id: number) => void;
  onStartClick: () => void;
}

export default class MenuView extends View {
  onFighterClick: (id: number) => void;
  onStartClick: () => void;
  constructor(selected: Array<Selected>) {
    super();
  }

  updateMenu(selected: Array<Selected>) {
    const menuElem = this._createMenu(selected);

    if (this.element) {
      this.element.replaceWith(menuElem);
    }
    this.element = menuElem;
  }

  _createMenu(selected: Array<Selected>) {
    const element = this.createElement({tagName: 'div', className: 'game-menu'});

    const startGameButton = this._createStartButton();
    if (selected[0] && selected[1]) {
      startGameButton.disabled = false;
    }
    const firstElem = this._createFighter(selected[0]);
    const secondElem = this._createFighter(selected[1]);
    
    element.append(firstElem, startGameButton, secondElem);
    return element;
  }

  private _createFighter(fighter: Selected) {
    const fighterElement = this.createElement({tagName: 'button', className: 'fighter-option'}) as HTMLButtonElement;

    if (!fighter) {
      fighterElement.innerText = 'Choose a fighter';
      fighterElement.disabled = true;
    } else {
      fighterElement.classList.add('selected');
      fighterElement.innerText = fighter.name;
      fighterElement.disabled = false;
    }

    fighterElement.onclick = (event) => {
      this.onFighterClick(fighter._id);
    }
 
    return fighterElement;
  }

  private _createStartButton() {
    const startButton = this.createElement({
      tagName: 'button',
      className: 'start-button'
    }) as HTMLButtonElement;
    startButton.innerText = 'Fight!';
    startButton.disabled = true;
    
    startButton.onclick = () => {
      this.onStartClick();
    }

    return startButton;
  }
}