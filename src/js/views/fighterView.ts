import View, { IView } from "./view";
import { IFighter } from "../fighter";

export interface IFighterView extends IView {
  element: HTMLElement;
  createFighter: (fighter: IFighter) => void;
}

export default class FighterView extends View {
  constructor(fighter: IFighter) {
    super();

    this.createFighter(fighter);
  }

  createFighter(fighter: IFighter) {
    const nameElement = this._createName(fighter.name); 
    const imageElement = this._createImage(fighter.source);

    const attributes = {'data-id': fighter._id};
    this.element = this.createElement({ tagName: 'div', className: 'fighter', attributes });
    this.element.append(imageElement, nameElement);
  }

  private _createName(name: string) {
    const nameElement = this.createElement({tagName: 'span', className: 'name'});
    nameElement.innerText = name;

    return nameElement;
  }

  private _createImage(source: string) {
    const attributes = {src: source};
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}