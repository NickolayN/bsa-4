import View from "./view";
import { IFighter } from "../fighter";
import FighterView from "./fighterView";

export interface IFightersView {
  element: HTMLElement;
  createFighters: (fighters: Array<IFighter>) => HTMLElement;
  onClick: (id: number) => number;
}

export default class FightersView extends View {
  element: HTMLElement;
  onClick(id: number): void {};

  constructor(fighters: Array<IFighter>) {
    super();

    this.createFighters(fighters);
  }

  createFighters(fighters: Array<IFighter>) {
    const fightersElement = fighters.map(fighter => {
      const fighterView = new FighterView(fighter);
      return fighterView.element;
    });

    this.element = this.createElement({tagName: 'div', className: 'fighters'});
    this.element.onclick = this._handleFighterClick.bind(this);
    this.element.append(...fightersElement);
  }

  private _handleFighterClick(event: Event): void {
    const eventTarget = event.target as HTMLElement;
    const target = eventTarget.closest('[data-id]') as HTMLElement | undefined;
    if(target) {
      const id = Number(target.dataset.id);
      this.onClick(id);
    }
  }
}