export function createInput(label: string, value: number) {
  const labelElement = document.createElement('label') as HTMLLabelElement;
  const inputElement = document.createElement('input') as HTMLInputElement;

  inputElement.id = label;
  inputElement.value = String(value);
  labelElement.textContent = label;
  labelElement.append(inputElement);

  return labelElement;
}