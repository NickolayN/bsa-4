import Fighter, { IFighter } from "../fighter";
import View from "./view";
import FighterView from "./fighterView";
import { createInput } from "./inputView";

export interface IModalView {
  element: HTMLElement;
  onSubmit: (fighter: IFighter) => IFighter;
}

export default class ModalView extends View implements IModalView {
  element: HTMLElement;
  onSubmit: (fighter: IFighter) => IFighter;

  constructor(fighter: IFighter) {
    super();
    this._createModal(fighter);
  }

  private _createModal(fighter: IFighter) {
    this.element = this.createElement({ tagName: 'div', className: 'modal' });
    const fighterView = new FighterView(fighter);
    const form = this._createForm(fighter);
    this.element.append(fighterView.element, form);
  }

  private _createForm(fighter: IFighter) {
    const formElement = this.createElement({
      tagName: 'form',
      className: 'modal-form'
    }) as HTMLFormElement;

    const attackInput = createInput('attack', fighter.attack);
    const defenseInput = createInput('defense', fighter.defense);
    const healthInput = createInput('health', fighter.health);
    const submit = this._createSubmit();
    formElement.append(healthInput, attackInput, defenseInput, submit);

    formElement.onsubmit = (event) => {
      event.preventDefault();

      const [attack, defense, health] = [
        formElement.attack.value,
        formElement.defense.value,
        formElement.health.value
      ]

      const data = { ...fighter, attack, defense, health};
      this._submitHandler(data)
    };
    return formElement;
  }

  private _submitHandler(fighter: IFighter) {
    this.onSubmit(fighter);
  }

  private _createSubmit() {
    const submit = this.createElement({ tagName: 'button', className: 'submit' }) as HTMLButtonElement;
    submit.textContent = 'Submit';
    return submit;
  }
}