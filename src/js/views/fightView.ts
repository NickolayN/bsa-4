import View, { IView } from "./view";
import { IFighter } from "../fighter";
import FighterView from "./fighterView";

export interface IFightView extends IView {
  hit: (damage: number) => void;
}

export default class FightView extends View implements IFightView {
  private _first: HTMLElement;
  private _second: HTMLElement;
  private _damage: HTMLElement;

  constructor(fighters: IFighter[]) {
    super();

    this.createFight(fighters);
  }

  createFight(fighters: IFighter[]) {
    this.element = this.createElement({
      tagName: 'div',
      className: 'fight'
    });

    [this._first, this._second] = fighters.map(fighter => new FighterView(fighter).element);
    this._second.classList.add('reverse');
    this.element.append(this._first, this._second);
  }

  hit(damage: number) {
    this._showDamage(damage);
  }

  private _showDamage(damage: number) {
    if (!this._damage) {
      this._damage = this.createElement({ tagName: 'span', className: 'damage' });
      this.element.append(this._damage);
    }

    this._damage.textContent = String(damage);
    
  }
}