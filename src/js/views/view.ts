type Element = {
  tagName: string;
  className: string;
  attributes?: {
    src?: string;
    'data-id'?: number;
  };
}

export interface IView {
  element: HTMLElement;

  createElement: (options: Element) => HTMLElement;
}

export default class View implements IView {
  element: HTMLElement;

  createElement(options: Element) {
    const element = document.createElement(options.tagName) as HTMLElement;
    element.classList.add(options.className);

    if (options.attributes) {
      Object.keys(options.attributes).forEach(key => {
        element.setAttribute(key, options.attributes[key]);
      });
    }

    return element;
  }
}