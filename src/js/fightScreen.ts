import Fighter, { IFighter } from "./fighter";
import { calculateDamage } from "./helpers/gameHelper";
import FightView, { IFightView } from "./views/fightView";

const ATTACK_TIME: number = 100;

type FightState = {
  firstFighterIsNext: boolean;
  firstFighterHealth: number,
  secondFighterHealth: number,
}

export interface IFight {
  element: HTMLElement;
  start: () => void;
}

export default class Fight {
  element: HTMLElement;
  private _state: FightState;
  private _fighters: Fighter[];
  private _view: IFightView;

  constructor(fighters: IFighter[]) {
    this._view = new FightView(fighters);
    this.element = this._view.element;

    this._fighters = fighters.map(fighter => new Fighter(fighter));
    this._state = {
      firstFighterIsNext: true,
      firstFighterHealth: fighters[0].health,
      secondFighterHealth: fighters[1].health,
    }
  }

  start() {
    this._round(this._fighters[0], this._fighters[1]);
  }

  private _round(attacker: Fighter, defender: Fighter) {
    const damage: number = calculateDamage(attacker, defender);
    this._view.hit(damage);

    if (this._state.firstFighterIsNext) {
      this._state = {
        ...this._state,
        secondFighterHealth: this._state.secondFighterHealth - damage
      }
    } else {
      this._state = {
        ...this._state,
        firstFighterHealth: this._state.firstFighterHealth - damage
      }
    }

    const result = this._checkWinner(this._state.firstFighterHealth, this._state.secondFighterHealth);
    
    if (result) {
      this._finish(result);
    } else {
      [attacker, defender] = [defender, attacker];
      this._state.firstFighterIsNext = !this._state.firstFighterIsNext;

      setTimeout(() => {
        this._round(attacker, defender);
      }, ATTACK_TIME);
    }
  }

  private _checkWinner(firstFighterHealth: number, secondFighterHealth: number): IFighter {
    if (firstFighterHealth <= 0) return this._fighters[1];
    if (secondFighterHealth <= 0) return this._fighters[0];
  }

  private _finish(result: IFighter) {
    alert(`Winner: ${result.name}`);
  }
}