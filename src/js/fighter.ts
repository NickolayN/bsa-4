export interface IFighter {
  readonly _id: number;
  readonly name: string;
  readonly source: string;
  health?: number;
  attack?: number;
  defense?: number;

  getHitPower: () => number;
  getBlockPower: () => number;
}

export default class Fighter implements IFighter {
  _id: number;
  name: string;
  source: string;
  health?: number;
  attack?: number;
  defense?: number;

  constructor(options: IFighter) {
    this._id = options._id;
    this.name = options.name;
    this.source = options.source;
    this.health = options.health;
    this.attack = options.attack;
    this.defense = options.defense;
  }

  getHitPower() {
    const criticalHitChance = this._getRandom(1, 2);
    const power = this.attack * criticalHitChance;
    return power;
  }

  getBlockPower() {
    const dodgeChance = this._getRandom(1, 2);
    const power = this.defense * dodgeChance;
    return power;
  }

  private _getRandom(min: number, max: number) {
    let rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
  }
}