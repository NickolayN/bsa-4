const presets = [
  "@babel/env",
  "@babel/typescript"
];

const plugins = [
  "@babel/plugin-proposal-class-properties",
  "@babel/proposal-object-rest-spread"
];

module.exports = {
  presets,
  plugins
};